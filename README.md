# CatalogSync backend

A node api server to sync between a local layers catalog [JSON array] and a remote layers catalog [JSON array].
The server checks the pre-sync status of layers and allows to sync each layer seperately or globally sync all the layers.