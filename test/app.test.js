const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const app = require("../index");

// configure chai
chai.use(chaiHttp);

describe("Catalogs", () => {
  describe("GET /", () => {
    // Test to get remote layers catalog
    it("should get remote layers catalog", (done) => {
      // catalog = an array of json objects with specific fields
      chai.request(app)
        .get("/catalogs/remote")
        .end((err, res) => {
          expect(res.status).to.equal(200);
          expect(res.body).to.be.an("array");
          res.body.forEach(layer => {
            expect(layer).to.be.an("object");
            expect(layer).to.have.property("id").that.is.a("string");
            expect(layer).to.have.property("name").that.is.a("string");
            expect(layer).to.have.property("type").that.is.a("string");
            expect(layer).to.have.property("version").that.is.a("number");
          });
          done();
        });
    });

    it("should get local layers catalog", (done) => {
      // catalog = an array of json objects with specific fields
      chai.request(app)
        .get("/catalogs/local")
        .end((err, res) => {
          expect(res.status).to.equal(200);
          expect(res.body).to.be.an("array");
          res.body.forEach(layer => {
            expect(layer).to.be.an("object");
            expect(layer).to.have.property("id").that.is.a("string");
            expect(layer).to.have.property("name").that.is.a("string");
            expect(layer).to.have.property("type").that.is.a("string");
            expect(layer).to.have.property("version").that.is.a("number");
          });
          done();
        });
    });
  });
});