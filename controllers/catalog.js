const path = require("path");
const { OK, BAD_REQUEST, INTERNAL_SERVER_ERROR } = require("http-status");
// CRUD handlers
const { addLayer, updateLayer, deleteLayer } = require("../helpers/crud");
// helper functions
const {
  getLayerById,
  findChanges,
  readCatalog,
  writeCatalog
} = require("../helpers/func");
const DATA_DIR = path.resolve("./data");

/** Greet user in root route. **/
function isServerUp(req, res) {
  res.end("layers catalog mock-api is up.");
}

/** Send remote catalog to user. */
async function getRemoteCatalog(req, res) {
  // Just a mock for now.
  try {
    const remoteCatalog = await readCatalog(`${DATA_DIR}/remoteCatalog.json`);
    res.json(remoteCatalog).status(OK);
  } catch (err) {
    console.log(err);
    res.status(INTERNAL_SERVER_ERROR);
  } finally {
    res.end();
  }
}

/** Send local catalog to user. */
async function getLocalCatalog(req, res) {
  // Just a mock for now.
  try {
    const localCatalog = await readCatalog(`${DATA_DIR}/localCatalog.json`);
    res.json(localCatalog).status(OK);
  } catch (err) {
    console.log(err);
    res.status(INTERNAL_SERVER_ERROR);
  } finally {
    res.end();
  }
}

/** Send pre-sync status to user specifying the changes needed
 *  in order to sync the local catalog with the remote catalog. */
async function getChanges(req, res) {
  try {
    const remoteCatalog = await readCatalog(`${DATA_DIR}/remoteCatalog.json`);
    const localCatalog = await readCatalog(`${DATA_DIR}/localCatalog.json`);
    const changes = findChanges(localCatalog, remoteCatalog);
    res.json(changes).status(OK);
  } catch (err) {
    console.log(err);
    res.status(INTERNAL_SERVER_ERROR);
  } finally {
    res.end();
  }
}

async function syncAll(req, res) {
  try {
    const remoteCatalog = await readCatalog(`${DATA_DIR}/remoteCatalog.json`);
    const localCatalog = await readCatalog(`${DATA_DIR}/localCatalog.json`);
    const changes = findChanges(localCatalog, remoteCatalog);

    let createFlag = false;
    let updateFlag = false;
    let deleteFlag = false;

    // update catalog given there are changes to sync
    if (changes.CREATE.length > 0) {
      createFlag = true;
      changes.CREATE.forEach(layer => addLayer(layer, localCatalog));
    }
    if (changes.UPDATE.length > 0) {
      updateFlag = true;
      changes.UPDATE.forEach(layer => {
        let remoteLayer = getLayerById(layer.id, remoteCatalog);
        updateLayer(remoteLayer, localCatalog);
      });
    }
    if (changes.DELETE.length > 0) {
      deleteFlag = true;
      changes.DELETE.forEach(layer => deleteLayer(layer, localCatalog));
    }
    // once local catalog has been updated - write to file and send server response
    if (createFlag || updateFlag || deleteFlag) {
      writeCatalog(
        `${DATA_DIR}/localCatalog.json`,
        JSON.stringify(localCatalog, null, 2)
      );
      res.status(OK).send("Local catalog has been updated.");
    } else res.status(BAD_REQUEST).send("No changes found.");
  } catch (err) {
    console.log(err);
    res.status(INTERNAL_SERVER_ERROR);
  } finally {
    res.end();
  }
}

async function syncLayer(req, res) {
  try {
    const remoteCatalog = await readCatalog(`${DATA_DIR}/remoteCatalog.json`);
    const localCatalog = await readCatalog(`${DATA_DIR}/localCatalog.json`);
    const changes = findChanges(localCatalog, remoteCatalog);
    /** Layer id param received in the request. */
    const layerId = req.params.layerId;

    /** layers ids array of the layers to be created locally during sync. */
    const createIds = changes.CREATE.map(layer => layer.id);
    /** Layers ids array of the layers to be updated locally during sync. */
    const updateIds = changes.UPDATE.map(layer => layer.id);
    /** Layers ids array of the layers to be deleted locally during sync. */
    const deleteIds = changes.DELETE.map(layer => layer.id);
    /** Layer ids array of layers in the local catalog. */
    const localIds = localCatalog.map(layer => layer.id);

    if (createIds.includes(layerId)) {
      // If request layer is in the CREATE list.
      const remoteLayer = getLayerById(layerId, remoteCatalog);
      addLayer(remoteLayer, localCatalog);
      res.status(OK).send("Layer has been added to local catalog.");
    } else if (updateIds.includes(layerId)) {
      // If request layer is in the UPDATE list.
      const remoteLayer = getLayerById(layerId, remoteCatalog);
      updateLayer(remoteLayer, localCatalog);
      res.status(OK).send("Layer has been updated in local catalog.");
    } else if (deleteIds.includes(layerId)) {
      // If request layer is in the DELETE list.
      const localLayer = getLayerById(layerId, localCatalog);
      deleteLayer(localLayer, localCatalog);
      res.status(OK).send("Layer has been deleted from local catalog.");
    } else if (localIds.includes(layerId)) {
      // If request layer is at least in the local catalog.
      res.status(BAD_REQUEST).send("No changes in layer.");
    } else {
      res.status(BAD_REQUEST).send("Layer does not even exist in local catalog.");
    }
  } catch (err) {
    console.log(err);
    res.status(INTERNAL_SERVER_ERROR);
  } finally {
    res.end();
  }
}

module.exports = {
  isServerUp,
  getRemoteCatalog,
  getLocalCatalog,
  getChanges,
  syncAll,
  syncLayer
};
