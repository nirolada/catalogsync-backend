const { compareByLayerId, getLayerById } = require("./func");

// CREATE
/**
 * Add a layer to a catalog.
 *
 * @param {Object} layer
 * @param {Object[]} catalog
 */
function addLayer(layer, catalog) {
  catalog.push(layer);
  catalog.sort(compareByLayerId);
}

// UPDATE
/**
 * Update a layer in a catalog.
 * 
 * @description Replace a layer in the given catalog with 
                the given layer assuming they both have the same id.
 * 
 * @param {Object} layer An up-to-date layer to update in the catalog.
 * @param {Object[]} catalog The catalog with the outdated layer.
 */
function updateLayer(layer, catalog) {
  const layerIndex = catalog.findIndex(
    catalogLayer => catalogLayer.id == layer.id
  );

  catalog.splice(layerIndex, 1, layer);
}

// DELETE
/**
 * Removes a layer from a catalog.
 *
 * @param {Object} layer A layer to remove from the catalog.
 * @param {Object[]} catalog The catalog from which we remove the layer.
 */
function deleteLayer(layer, catalog) {
  const layerIndex = catalog.findIndex(
    catalogLayer => catalogLayer.id == layer.id
  );

  catalog.splice(layerIndex, 1);
}

module.exports = {
  addLayer,
  updateLayer,
  deleteLayer
};
