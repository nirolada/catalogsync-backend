const fs = require("fs").promises;
/**
 * Checks if a layer exists in a catalog based on layers ids.
 *
 * @param {Object} layer A layer to check.
 * @param {Object[]} catalog The catalog that should include the layer.
 *
 * @returns {boolean}
 */
function isLayerInCatalog(layer, catalog) {
  let catalogLayersIds = catalog.map(catalogLayer => catalogLayer.id);
  return catalogLayersIds.includes(layer.id);
}

/**
 * Checks if a layer is up to date according to an up-to-date catalog.
 *
 * @param {Object} layer A layer to check.
 * @param {Object[]} catalog The up-to-date layers catalog.
 *
 * @return {boolean}
 */
function isLayerUpToDate(layer, catalog) {
  let twinInCatalog = catalog.find(
    catalogLayer => catalogLayer.id === layer.id
  );
  return twinInCatalog.version == layer.version;
}

/**
 * Get a layer from a catalog by its id.
 *
 * @param {string} layerId
 * @param {Object[]} catalog
 *
 * @return {Object} Layer
 */
function getLayerById(layerId, catalog) {
  return catalog.find(layer => layer.id == layerId);
}

/** Compare local catalog to remote catalog to find changes for sync.
 * @param {Object[]} localCatalog Local catalog of layers.
 * @param {Object[]} remoteCatalog Remote catalog of layers.
 * @return {Object[]} A json containing the changes to make in local
 *                    in order to sync between the catalogs.
 */
function findChanges(localCatalog, remoteCatalog) {
  /** Layers created in remote but still dont exist in local. */
  const CREATE = remoteCatalog.filter(
    layer => !isLayerInCatalog(layer, localCatalog)
  );

  /** Outdated layers in local catalog. */
  const UPDATE = localCatalog
    .filter(layer => isLayerInCatalog(layer, remoteCatalog))
    .filter(layer => !isLayerUpToDate(layer, remoteCatalog));

  /** Layers deleted from remote but still exist in local. */
  const DELETE = localCatalog.filter(
    layer => !isLayerInCatalog(layer, remoteCatalog)
  );

  return {
    CREATE,
    UPDATE,
    DELETE
  };
}

function compareByLayerId(a, b) {
  if (a.id > b.id) return 1;
  if (a.id < b.id) return -1;
  return 0;
}

async function readCatalog(filePath) {
  let catalog;
  await fs
    .readFile(filePath)
    .then(data => {
      try {
        catalog = JSON.parse(data);
      } catch (err) {
        console.log("Error parsing catalog file to JSON -", err);
      }
    })
    .catch(err => {
      console.log("Error reading catalog file -", err);
      throw err;
    });

  return catalog;
}

async function writeCatalog(catalogPath, catalog) {
  await fs
    .writeFile(catalogPath, catalog)
    .then(() => console.log("Done writing catalog file."))
    .catch(err => {
      console.log("Error writing catalog file -", err);
      throw err;
    });
}

module.exports = {
  compareByLayerId,
  findChanges,
  getLayerById,
  readCatalog,
  writeCatalog
  //   isLayerInCatalog,
  //   isLayerUpToDate,
};
