const express = require("express");
const {
  isServerUp,
  getRemoteCatalog,
  getLocalCatalog,
  getChanges,
  syncAll,
  syncLayer
} = require("../controllers/catalog");

const router = express.Router();

// welcome message
router.get("/", isServerUp);
// get catalogs
router.get("/catalogs/remote", getRemoteCatalog);
router.get("/catalogs/local", getLocalCatalog);
// local catalog CRUD sync
router.get("/catalogs/changes", getChanges);
router.get("/catalogs/sync", syncAll);
router.get("/catalogs/sync/:layerId", syncLayer);

module.exports = router;
