const express = require("express");
const app = express();
const morgan = require("morgan");

// routes
const catalogRoutes = require("./routes/catalog");

// middleware
app.use(morgan("dev"));
app.use("/", catalogRoutes);

// start the server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`A node.js API is listening on port ${PORT}`);
});

module.exports = app;